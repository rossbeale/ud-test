# UNiDAYS Test Project #

## Setup ##

You'll need Xcode.

Dependencies are included in the Git repo for ease. `git clone` and running the project should be enough.  If any trouble, ensure [Carthage](https://github.com/Carthage/Carthage) is used and run `carthage update`.

## Testing ##

A simple UI test is included to prevent regressions during development.  Run with `CMD + U` in Xcode.

## Assumptions, Caveats & Future TODOs ##

- Assumptions were made regarding providing feedback of upload success.  Presented currently in `UIAlertController`, with pending total always present on upload screen.
- Assumption made over user journey when adding a student: saved with an attempt to upload.  Object is persisted regardless.
- Clean up Realm objects as with current design, they are not actually needed post-upload.
- Ideally add local notifications regarding failure.
- Ideally add background refresh support to try sync again later in the background.
- More unit tests to model logic.
- Voiceover and other accessibility support.
- Network layer is left as a `TODO` (return `false` in the relevant method in `Network.swift` to incur failure conditions)