//
//  UITests.swift
//  UITests
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import XCTest

class UITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = true
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testStudentAdd() {
        let app = XCUIApplication()
        app.staticTexts["ADD STUDENT"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        elementsQuery.textFields["First Name"].typeText("Ross")
        elementsQuery.textFields["Last Name"].tap()
        elementsQuery.textFields["Last Name"].typeText("Beale")
        elementsQuery.textFields["Gender"].tap()
        elementsQuery.textFields["Email Address"].tap()
        elementsQuery.textFields["Email Address"].typeText("rossbeale@gmail.com")
        elementsQuery.textFields["University"].tap()
        app.toolbars.buttons["Done"].tap()
        
        let saveUploadButton = app.buttons["Save & Upload"]
        saveUploadButton.tap()
        
        // determine save button worked
        XCTAssert(app.alerts["Saved"].exists)
        app.alerts["Saved"].buttons["OK"].tap()
        
        // determine upload happened
        waitForElementToAppear(element: app.alerts["Upload Result"])
        waitForElementToAppear(element: app.staticTexts["NONE PENDING"])
    }
    
    private func waitForElementToAppear(element: XCUIElement,
                                        file: String = #file, line: UInt = #line) {
        let existsPredicate = NSPredicate(format: "exists == true")
        expectation(for: existsPredicate, evaluatedWith: element, handler: nil)
        
        waitForExpectations(timeout: 5) { (error) -> Void in
            if (error != nil) {
                let message = "Failed to find \(element) after 5 seconds."
                self.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
            }
        }
    }
    
}
