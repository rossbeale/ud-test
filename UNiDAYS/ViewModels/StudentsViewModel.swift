//
//  StudentsViewModel.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Async
import Foundation

typealias UploadClosureType = (_ message: String) -> (Void)

class StudentsViewModel {
    
    var objectStore: ObjectStore
    var uploading = false
    
    init(objectStore: ObjectStore) {
        self.objectStore = objectStore
    }
    
}

// MARK: Upload

extension StudentsViewModel {
    
    func triggerUpload(completion: @escaping UploadClosureType) {
        if self.uploading {
            return
        }
        
        uploading = true
        
        // async dispatch each item
        var successfulCount = 0
        var failedCount = 0
        
        // perform all in bg
        Async.background {
        
            let os = try! ObjectStore()
            let studentIds = self.fetchStudentsAwaitingUpload(withObjectStore: os).map({ $0.objectId })
            guard studentIds.count > 0 else {
                Async.main {
                    completion("No students were uploaded.")
                }
                return
            }
            
            let group = AsyncGroup()
            for studentId in studentIds {
                group.enter()
                group.background {
                    // fetch student
                    let os = try! ObjectStore()
                    if let student = os.object(ofType: Student.self, forPrimaryKey: studentId) {
                        Network.postStudentDictionary(dictionary: student.mappedDictionary) { success in
                            if success {
                                successfulCount = successfulCount + 1
                                // refresh to stop invalidation
                                if let student = os.object(ofType: Student.self, forPrimaryKey: studentId) {
                                    try! os.write {
                                        student.uploadedAt = Date()
                                    }
                                }
                            } else {
                                failedCount = failedCount + 1
                            }
                            
                            group.leave()
                        }
                    }
                }
            }
            
            group.wait()
         
        }.main(after: 0.1) {
            
            var msg = ""
            if successfulCount > 0 {
                let pluralised = successfulCount == 1 ? "student" : "students"
                msg += "\(successfulCount) \(pluralised) were uploaded. "
            }
            if failedCount > 0 {
                let pluralised = successfulCount == 1 ? "student" : "students"
                msg += "\(successfulCount) \(pluralised) failed to upload. "
            }
            
            self.uploading = false
            completion(msg)
            
        }
    }
    
}

// MARK: Utils

extension StudentsViewModel {
    
    func fetchStudentsAwaitingUpload() -> [Student] {
        return fetchStudentsAwaitingUpload(withObjectStore: self.objectStore)
    }
    
    func fetchStudentsAwaitingUpload(withObjectStore objectStore: ObjectStore) -> [Student] {
        return Array(objectStore.objects(Student.self).filter("uploadedAt == nil"))
    }
    
}
