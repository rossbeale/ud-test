//
//  StudentViewModel.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation

struct TransientStudentInput {
    let firstName: String
    let lastName: String
    let gender: Gender?
    let email: String
    let institution: Institution?
}

struct StudentViewModel {
    
    var objectStore: ObjectStore
    
    // utils
    lazy var possibleInstitutions: [Institution] = {
        return Institution.loadInstitutionsFromMainBundle()
    }()
    lazy var possibleGenders: [Gender] = {
        return Gender.loadGendersFromMainBundle()
    }()
    
    init(objectStore: ObjectStore) {
        self.objectStore = objectStore
    }
    
    func performSave(transientStudent: TransientStudentInput) -> (success: Bool, errorMessage: String?) {
        let validation = performValidation(transientStudent: transientStudent)
        guard validation.passedValidation else {
            return (false, validation.validationMessage)
        }
        
        let student = Student()
        student.firstName = transientStudent.firstName
        student.lastName = transientStudent.lastName
        student.gender = transientStudent.gender!.value
        student.email = transientStudent.email
        student.institution = transientStudent.institution!.name
        
        do {
            try objectStore.write {
                objectStore.add(student)
            }
        } catch {
            return (false, "Could not be saved due to database error")
        }
        
        return (true, nil)
    }
    
    private func performValidation(transientStudent: TransientStudentInput) -> (passedValidation: Bool, validationMessage: String?) {
        var invalidInputs = [String]()
        
        if transientStudent.firstName.isEmpty {
            invalidInputs.append("First name")
        }
        if transientStudent.lastName.isEmpty {
            invalidInputs.append("Last name")
        }
        if transientStudent.gender == nil {
            invalidInputs.append("Gender")
        }
        if transientStudent.email.isEmpty {
            invalidInputs.append("Email address")
        }
        if transientStudent.institution == nil {
            invalidInputs.append("University")
        }
        
        if invalidInputs.count > 0 {
            var message = "Missing inputs:\n"
            for input in invalidInputs {
                message += "\n - \(input) is blank."
            }
            
            return (false, message)
        } else {
            return (true, nil)
        }
    }
    
}
