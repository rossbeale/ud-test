//
//  Gender.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation

struct Gender {
    
    let display: String
    let value: String
    
    init(json: [String : Any]) {
        display = json["display"] as! String
        value = json["value"] as! String
    }
    
}

extension Gender {
    
    static func loadGendersFromMainBundle() -> [Gender] {
        let path = Bundle.main.path(forResource: "genders", ofType: "json")!
        return loadGenders(path: path)
    }
    
    private static func loadGenders(path: String) -> [Gender] {
        // parse from file
        do {
            let data = try NSData(contentsOfFile: path, options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as? Array<[String : Any]>
            // objs
            if let json = json {
                return json.map { Gender(json: $0) }
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
}
