//
//  Student.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation
import RealmSwift

class Student: Object {
    
    // attributes
    dynamic var objectId = NSUUID().uuidString
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var gender = ""
    dynamic var institution = ""
    dynamic var email = ""
    dynamic var uploadedAt: Date?
    
    override class func primaryKey() -> String {
        return "objectId"
    }
    
}

// MARK: Utilities

extension Student {
    
    var mappedDictionary: Dictionary<String, Any> {
        return [
            "first_name" : firstName,
            "last_name" : lastName,
            "gender" : gender,
            "university" : institution,
            "email" : email
        ]
    }
    
}
