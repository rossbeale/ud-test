//
//  University.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation

struct Institution {
    
    let name: String
    let region: String
    
    init(json: [String : Any]) {
        name = json["university"] as! String
        region = json["region"] as! String
    }
    
}

extension Institution {
    
    static func loadInstitutionsFromMainBundle() -> [Institution] {
        let path = Bundle.main.path(forResource: "institutions", ofType: "json")!
        return loadInstitutions(path: path)
    }
    
    private static func loadInstitutions(path: String) -> [Institution] {
        // parse from file
        do {
            let data = try NSData(contentsOfFile: path, options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as? Array<[String : Any]>
            // objs
            if let json = json {
                return json.map { Institution(json: $0) }.sorted(by: { (i1, i2) -> Bool in
                    return i1.name < i2.name
                })
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
}
