//
//  Network.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation

typealias NetworkClosureType = (_ success: Bool) -> (Void)

struct Network {
    
    static func postStudentDictionary(dictionary: Dictionary<String, Any>, completion: NetworkClosureType) {
        // TODO: determine upload path, past to Moya/Alamofire/Custom Network Layer
        sleep(1)
        completion(true)
    }
    
}
