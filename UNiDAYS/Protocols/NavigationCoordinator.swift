//
//  NavigationCoordinator.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation
import UIKit

class NavigationCoordinator {
    
    // utils
    var window: UIWindow!
    var storyboard: UIStoryboard!
    var navigationController: UINavigationController!
    
    private let storyboardIdentifier = "Main"
    
    init() {
        storyboard = {
            return UIStoryboard(name: storyboardIdentifier, bundle: nil)
        }()
        
        // NOTE: you may be wondering why I am instantiated this in code. It allows greater flexibility in the future so we can dynamically change the initial view controller.
        // Storing the references to "managing vcs" (the navigation controller in this case), means we can later alter it.  For example, when the user logs out, we can swap it out with ease.
        
        navigationController = {
            // initial
            let initialController = storyboard.instantiateViewController(withIdentifier: String(describing: UploadViewController.self)) as! UploadViewController
            // nvc
            let navigationController = UINavigationController(rootViewController: initialController)
            navigationController.navigationBar.barTintColor = UIColor.unidaysBeige
            navigationController.navigationBar.isTranslucent = false
            return navigationController
        }()
        
        window = {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            return window
        }()
    }
    
}

// MARK: Utilities

extension NavigationCoordinator {
    
    func push(viewControllerIdentifier: String) {
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func pop() {
        navigationController.popViewController(animated: true)
    }
    
}
