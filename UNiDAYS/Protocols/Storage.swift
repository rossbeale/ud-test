//
//  Storage.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import Foundation
import RealmSwift

typealias ObjectStore = Realm

class Storage {
    
    static var objectStore: ObjectStore = {
        return try! ObjectStore()
    }()
    
}
