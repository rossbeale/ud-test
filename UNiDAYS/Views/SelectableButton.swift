//
//  SelectableButton.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import UIKit

class SelectableButton: UIButton {
    
    @IBInspectable var standardColor: UIColor!
    @IBInspectable var selectedColor: UIColor!
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isSelected || isHighlighted ? selectedColor : standardColor
        }
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected || isHighlighted ? selectedColor : standardColor
        }
    }

}
