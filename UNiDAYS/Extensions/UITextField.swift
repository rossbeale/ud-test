//
//  UITextField.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import UIKit

extension UITextField {
    
    var hasInput: Bool {
        return !strippedText.isEmpty
    }
    
    var strippedText: String {
        if let str = text {
            return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        
        return ""
    }
    
}
