//
//  UIPickerView.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import UIKit

extension UIPickerView {
    
    static func pickerViewWithToolbar(delegate: UIPickerViewDelegate, nextTarget: Any, nextSelector: Selector) -> (pickerView: UIPickerView, toolbar: UIToolbar) {
        
        let pickerView = UIPickerView()
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = delegate
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: nextTarget, action: nextSelector)
        toolbar.setItems([space, doneButton], animated: false)
        
        return (pickerView, toolbar)
    }
    
}
