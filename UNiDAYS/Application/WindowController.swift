//
//  AppDelegate.swift
//  UNiDAYS
//
//  Created by Ross Beale on 9/24/16
//  Copyright (c) 2016 UNiDAYS. All rights reserved.
//

import UIKit

@UIApplicationMain
class WindowController: UIResponder, UIApplicationDelegate {
    
    var navigationCoordinator: NavigationCoordinator!
    
}

// MARK: Class Utilities

extension WindowController {
    
    class func windowController() -> WindowController {
        return UIApplication.shared.delegate as! WindowController
    }
    
}

// MARK: UIApplicationDelegate

extension WindowController {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        // setup navigation
        navigationCoordinator = NavigationCoordinator()
        
        return true
    }
    
}
