//
//  CreateStudentViewController.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import UIKit

class CreateStudentViewController: UIViewController {

    // UI
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var genderTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var institutionTextField: UITextField!
    
    // Data (UI)
    var genderPickerView: UIPickerView!
    var institutionPickerView: UIPickerView!
    
    // Data
    var viewModel: StudentViewModel!// vars
    var selectedGender: Gender?
    var selectedInstitution: Institution?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // vm
        let store = Storage.objectStore
        viewModel = StudentViewModel(objectStore: store)
        
        // setup pickers
        let genderPickerElements = UIPickerView.pickerViewWithToolbar(delegate: self, nextTarget: self, nextSelector: #selector(pickerDonePressed))
        genderPickerView = genderPickerElements.pickerView
        genderTextField.inputView = genderPickerView
        genderTextField.inputAccessoryView = genderPickerElements.toolbar
        // hide cursor
        genderTextField.tintColor = .clear
        
        let institutionPickerElements = UIPickerView.pickerViewWithToolbar(delegate: self, nextTarget: self, nextSelector: #selector(pickerDonePressed))
        institutionPickerView = institutionPickerElements.pickerView
        institutionTextField.inputView = institutionPickerView
        institutionTextField.inputAccessoryView = institutionPickerElements.toolbar
        institutionTextField.tintColor = .clear
        
        registerForKeyboardNotifications()
    }
    
    deinit {
        viewModel = nil
        
        unregisterForKeyboardNotifications()
    }
    
}

// MARK: Lifecycle

extension CreateStudentViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        firstNameTextField.becomeFirstResponder()
    }
    
}

// MARK: Keyboard Notifications

extension CreateStudentViewController {
    
    func registerForKeyboardNotifications() {
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        nc.addObserver(self, selector: #selector(keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unregisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification: NSNotification) {
        let info = notification.userInfo!
        
        let keyboardHeight = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt

        let options = UIViewAnimationOptions(rawValue: curve << 16)
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: options,
                       animations: {
                        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
            },
                       completion: nil)
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        let info = notification.userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        
        let options = UIViewAnimationOptions(rawValue: curve << 16)
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: options,
                       animations: {
                        self.scrollView.contentInset = UIEdgeInsets.zero
            },
                       completion: nil)
    }
    
}

// MARK: UITextFieldDelegate

extension CreateStudentViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case genderTextField:
            if !genderTextField.hasInput {
                selectedGender = viewModel.possibleGenders.first
                genderTextField.text = selectedGender?.display
            }
            break
        case institutionTextField:
            if !institutionTextField.hasInput {
                selectedInstitution = viewModel.possibleInstitutions.first
                institutionTextField.text = selectedInstitution?.name
            }
            break
        default:
            ()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
            break
        case lastNameTextField:
            genderTextField.becomeFirstResponder()
            break
        case genderTextField:
            emailTextField.becomeFirstResponder()
            break
        case emailTextField:
            institutionTextField.becomeFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}

// MARK: UIPickerViewDelegate

extension CreateStudentViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPickerView {
            return viewModel.possibleGenders.count
        } else if pickerView == institutionPickerView {
            return viewModel.possibleInstitutions.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderPickerView {
            return viewModel.possibleGenders[row].display
        } else if pickerView == institutionPickerView {
            return viewModel.possibleInstitutions[row].name
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPickerView {
            selectedGender = viewModel.possibleGenders[row]
            genderTextField.text = selectedGender?.display
        } else if pickerView == institutionPickerView {
            selectedInstitution = viewModel.possibleInstitutions[row]
            institutionTextField.text = selectedInstitution?.name
        }
    }
    
    func pickerDonePressed() {
        if genderTextField.isFirstResponder {
            emailTextField.becomeFirstResponder()
        } else if institutionTextField.isFirstResponder {
            institutionTextField.resignFirstResponder()
        }
    }
    
}

// MARK: Actions

extension CreateStudentViewController {

    @IBAction func saveButtonPressed(_ sender: UIButton) {
        // create transient obj
        let si = TransientStudentInput(firstName: firstNameTextField.strippedText, lastName: lastNameTextField.strippedText, gender: selectedGender, email: emailTextField.strippedText, institution: selectedInstitution)
        
        // save it
        let result = viewModel.performSave(transientStudent: si)
        if result.success {
            let ac = UIAlertController(title: "Saved", message: "This student has been saved.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default) { action in
                WindowController.windowController().navigationCoordinator.pop()
            })
            present(ac, animated: true)
        } else {
            var message = result.errorMessage
            if message == nil {
                message = "An unexplained error occured."
            }
            
            let ac = UIAlertController(title: "Not Saved", message: message, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
}
