//
//  UploadViewController.swift
//  UNiDAYS
//
//  Created by Ross Beale on 24/09/2016.
//  Copyright © 2016 UNiDAYS. All rights reserved.
//

import UIKit

class UploadViewController: UIViewController {

    // UI
    @IBOutlet var pendingUploadsLabel: UILabel!
    
    // Data
    var viewModel: StudentsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let objStore = Storage.objectStore
        viewModel = StudentsViewModel(objectStore: objStore)
    }
    
    deinit {
        viewModel = nil
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        calculatePendingUploadsLabel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // trigger automatically
        uploadIfNeeded()
    }
    
}

// MARK: Utility methods

extension UploadViewController {
    
    fileprivate func uploadIfNeeded() {
        let students = viewModel.fetchStudentsAwaitingUpload()
        if students.count > 0 {
            upload()
        }
    }
    
    fileprivate func upload() {
        pendingUploadsLabel.text = "SUBMITTING..."
        
        viewModel.triggerUpload { message in
            self.calculatePendingUploadsLabel()
            
            let ac = UIAlertController(title: "Upload Result", message: message, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
    
    fileprivate func calculatePendingUploadsLabel() {
        let students = viewModel.fetchStudentsAwaitingUpload()
        if students.count > 0 {
            pendingUploadsLabel.text = "\(students.count) PENDING"
        } else {
            pendingUploadsLabel.text = "NONE PENDING"
        }
    }
    
}

// MARK: Actions

extension UploadViewController {
    
    @IBAction func uploadButtonPressed(_ sender: SelectableButton) {
        upload()
    }
    
    @IBAction func addNewStudentButtonPressed(_ sender: SelectableButton) {
        let vc = String(describing: CreateStudentViewController.self)
        WindowController.windowController().navigationCoordinator.push(viewControllerIdentifier: vc)
    }

}
